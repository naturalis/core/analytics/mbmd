package rs485


// url to https://www.controlin.com/nl/producten/energiemeters/poweranalysers/skd-103-m/
// https://www.controlin.com/wp-content/uploads/2021/08/skd-103-m-energiemeters-controlin-en-pro-modbus-2021.pdf

import (
	. "github.com/volkszaehler/mbmd/meters"
)

func init() {
	Register("SKD103M", NewSKD103MProducer)
}

type SKD103MProducer struct {
	Opcodes
}

func NewSKD103MProducer() Producer {
	ops := Opcodes{
		VoltageL1:    0x0000,
		VoltageL2:    0x0002,
		VoltageL3:    0x0004,
		CurrentL1:    0x0006, 
		CurrentL2:    0x0008,
		CurrentL3:    0x000A, 
		PowerL1:      0x000C,   
		PowerL2:      0x000E,   
		PowerL3:      0x0010,   
		Frequency:    0x0046,
		Sum:	      0x0048, 
	}
	return &SKD103MProducer{Opcodes: ops}
}

// Description implements Producer interface
func (p *SKD103MProducer) Description() string {
	return "SKD103M meter from Controlin"
}

func (p *SKD103MProducer) snip(iec Measurement, readlen uint16, transform RTUTransform, scaler ...float64) Operation {
	snip := Operation{
//		FuncCode:  ReadHoldingReg,
		FuncCode:  ReadInputReg,
//		OpCode:    p.Opcodes[iec]-1, // adjust documentation -1
		OpCode:    p.Opcodes[iec], // just like in the documentation
		ReadLen:   readlen,
		Transform: transform,
		IEC61850:  iec,
	}

	if len(scaler) > 0 {
		snip.Transform = MakeScaledTransform(snip.Transform, scaler[0])
	}

	return snip
}

// snip16 creates modbus operation for single register
func (p *SKD103MProducer) snip16i(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 1, RTUInt16ToFloat64, scaler...)
}

// snip32u creates modbus operation for double register
func (p *SKD103MProducer) snip16u(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 2, RTUUint32ToFloat64, scaler...)
}

// snip32u creates modbus operation for double register
func (p *SKD103MProducer) snip32u(iec Measurement, scaler ...float64) Operation {
	return p.snip(iec, 2, RTUUint32ToFloat64, scaler...)
}

// snip32i creates modbus operation for double register
func (p *SKD103MProducer) snip32i(iec Measurement, scaler ...float64) Operation {
	return p.snip(iec, 2, RTUInt32ToFloat64, scaler...)
}

// snip32f creates modbus operation for double register
func (p *SKD103MProducer) snip32f(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 2, RTUIeee754ToFloat64, scaler...)
}

// Probe implements Producer interface
func (p *SKD103MProducer) Probe() Operation {
	return p.snip32f(VoltageL1, 1)
}

// Produce implements Producer interface
func (p *SKD103MProducer) Produce() (res []Operation) {
	  for _, op := range []Measurement{
		  // VoltageL1,VoltageL2,VoltageL3,Power,Frequency,ApparentPower,CurrentL1,CurrentL2,CurrentL3,
		  VoltageL1, VoltageL2, VoltageL3, 
		  CurrentL1, CurrentL2, CurrentL3,
		  PowerL1, PowerL2, PowerL3,
		  Sum, Frequency,
        } {
		res = append(res, p.snip32f(op, 1))
        }
/*
	for _, op := range []Measurement{
		Current,Power,Sum,
               
	} {
		res = append(res, p.snip32i(op, 10))
	}
	*/
	return res
}

