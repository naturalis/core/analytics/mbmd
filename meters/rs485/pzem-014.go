package rs485


// url to documentation https://images-na.ssl-images-amazon.com/images/I/81GtkIOyZaL.pdf


import (
	. "github.com/volkszaehler/mbmd/meters"
)

func init() {
	Register("PZEM014", NewPZEM014Producer)
}

type PZEM014Producer struct {
	Opcodes
}

func NewPZEM014Producer() Producer {
	ops := Opcodes{
		VoltageL1:    0x0000,
		Voltage:      0x0000,
		Current:      0x0001, 
		Power:        0x0003,   
		Sum:	      0x0005, 
		Frequency:    0x0007,
//		PowerFactorValue: 0x0008, // net implemented yet
//		Alarm:        0x0009.
	}
	return &PZEM014Producer{Opcodes: ops}
}

// Description implements Producer interface
func (p *PZEM014Producer) Description() string {
	return "PZEM014 meter from China"
}

func (p *PZEM014Producer) snip(iec Measurement, readlen uint16, transform RTUTransform, scaler ...float64) Operation {
	snip := Operation{
//		FuncCode:  ReadHoldingReg,
		FuncCode:  ReadInputReg,
//		OpCode:    p.Opcodes[iec]-1, // adjust documentation -1
		OpCode:    p.Opcodes[iec], // just like in the documentation
		ReadLen:   readlen,
		Transform: transform,
		IEC61850:  iec,
	}

	if len(scaler) > 0 {
		snip.Transform = MakeScaledTransform(snip.Transform, scaler[0])
	}

	return snip
}

// snip16 creates modbus operation for single register
func (p *PZEM014Producer) snip16i(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 1, RTUInt16ToFloat64, scaler...)
}

// snip32u creates modbus operation for double register
func (p *PZEM014Producer) snip16u(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 2, RTUUint32ToFloat64, scaler...)
}

// snip32u creates modbus operation for double register
func (p *PZEM014Producer) snip32u(iec Measurement, scaler ...float64) Operation {
	return p.snip(iec, 2, RTUUint32ToFloat64, scaler...)
}

// snip32i creates modbus operation for double register
func (p *PZEM014Producer) snip32i(iec Measurement, scaler ...float64) Operation {
	return p.snip(iec, 2, RTUInt32ToFloat64, scaler...)
}

// snip32f creates modbus operation for double register
func (p *PZEM014Producer) snip32f(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 2, RTUIeee754ToFloat64, scaler...)
}

// Probe implements Producer interface
func (p *PZEM014Producer) Probe() Operation {
	return p.snip16i(VoltageL1, 10)
}

// Produce implements Producer interface
func (p *PZEM014Producer) Produce() (res []Operation) {
	  for _, op := range []Measurement{
		  // VoltageL1,VoltageL2,VoltageL3,Power,Frequency,ApparentPower,CurrentL1,CurrentL2,CurrentL3,
		  Voltage,Frequency,
        } {
		res = append(res, p.snip16i(op, 10))
        }

	for _, op := range []Measurement{
		Current,Power,Sum,
               
	} {
		res = append(res, p.snip32i(op, 10))
	}
	return res
}
