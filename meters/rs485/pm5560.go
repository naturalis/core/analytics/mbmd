package rs485


// url to documentation https://www.se.com/be/nl/product/METSEPM5560

import (
	. "github.com/volkszaehler/mbmd/meters"
)

func init() {
	Register("PM5560", NewPM5560Producer)
}

type PM5560Producer struct {
	Opcodes
}

func NewPM5560Producer() Producer {
	/***
	 https://ckm-content.se.com/ckmContent/sfc/servlet.shepherd/document/download/0691H00000H1NDBQA3
	*/
	ops := Opcodes{
		VoltageL1:       3028,
		VoltageL2:       3030,
		VoltageL3:       3032,
		Power: 2700,    // ActivePowerTotal
		// ActivePowerLastDemand: 3060, // ActivePowerLastDemand
		ApparentPower: 3076, // ApparentPowerTotal
		CurrentL1: 3000,
		CurrentL2: 3002,
		CurrentL3: 3004,
		Frequency:       3110,
	}
	return &PM5560Producer{Opcodes: ops}
}

// Description implements Producer interface
func (p *PM5560Producer) Description() string {
	return "PM5560  meter from Schneider Electic"
}

func (p *PM5560Producer) snip(iec Measurement, readlen uint16, transform RTUTransform, scaler ...float64) Operation {
	snip := Operation{
		FuncCode:  ReadHoldingReg,
		OpCode:    p.Opcodes[iec]-1,
		ReadLen:   readlen,
		Transform: transform,
		IEC61850:  iec,
	}

	if len(scaler) > 0 {
		snip.Transform = MakeScaledTransform(snip.Transform, scaler[0])
	}

	return snip
}

func (p *PM5560Producer) snip16u(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 2, RTUUint32ToFloat64, scaler...)
}

// snip32u creates modbus operation for double register
func (p *PM5560Producer) snip32u(iec Measurement, scaler ...float64) Operation {
	return p.snip(iec, 2, RTUUint32ToFloat64, scaler...)
}

// snip32i creates modbus operation for double register
func (p *PM5560Producer) snip32i(iec Measurement, scaler ...float64) Operation {
	return p.snip(iec, 2, RTUInt32ToFloat64, scaler...)
}

// snip32f creates modbus operation for double register
func (p *PM5560Producer) snip32f(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 2, RTUIeee754ToFloat64, scaler...)
}

// Probe implements Producer interface
func (p *PM5560Producer) Probe() Operation {
	return p.snip32u(VoltageL1, 10)
}

// Produce implements Producer interface
func (p *PM5560Producer) Produce() (res []Operation) {
	  for _, op := range []Measurement{
		  Power,Frequency,ApparentPower,CurrentL1,CurrentL2,CurrentL3,
        } {
                res = append(res, p.snip32f(op, 1))
        }

	for _, op := range []Measurement{
		VoltageL1, VoltageL2, VoltageL3,
	} {
		res = append(res, p.snip32u(op, 10000000))
	}
/*
	for _, op := range []Measurement{
		CurrentL1, CurrentL2, CurrentL3,
		Import, Export, ReactiveSum, Frequency,
	} {
		res = append(res, p.snip32u(op, 100))
	}

	for _, op := range []Measurement{
		Sum,
		Power, PowerL1, PowerL2, PowerL3,
		ReactivePower, ReactivePowerL1, ReactivePowerL2, ReactivePowerL3,
	} {
		res = append(res, p.snip32i(op, 100))
	}

	for _, op := range []Measurement{
		Cosphi, CosphiL1, CosphiL2, CosphiL3,
	} {
		res = append(res, p.snip32u(op, 1000))
	}
*/
	return res
}
