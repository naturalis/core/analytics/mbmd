package rs485


// url to https://www.controlin.com/nl/producten/energiemeters/poweranalysers/skd-045-m/


import (
	. "github.com/volkszaehler/mbmd/meters"
)

func init() {
	Register("SKD045M", NewSKD045MProducer)
}

type SKD045MProducer struct {
	Opcodes
}

func NewSKD045MProducer() Producer {
	ops := Opcodes{
		VoltageL1:    0x0000,
		Voltage:      0x0000,
		Current:      0x0006, 
		Power:        0x000C,   
		Frequency:    0x0046,
		Sum:	      0x0048, 
	}
	return &SKD045MProducer{Opcodes: ops}
}

// Description implements Producer interface
func (p *SKD045MProducer) Description() string {
	return "SKD045M meter from Controlin"
}

func (p *SKD045MProducer) snip(iec Measurement, readlen uint16, transform RTUTransform, scaler ...float64) Operation {
	snip := Operation{
//		FuncCode:  ReadHoldingReg,
		FuncCode:  ReadInputReg,
//		OpCode:    p.Opcodes[iec]-1, // adjust documentation -1
		OpCode:    p.Opcodes[iec], // just like in the documentation
		ReadLen:   readlen,
		Transform: transform,
		IEC61850:  iec,
	}

	if len(scaler) > 0 {
		snip.Transform = MakeScaledTransform(snip.Transform, scaler[0])
	}

	return snip
}

// snip16 creates modbus operation for single register
func (p *SKD045MProducer) snip16i(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 1, RTUInt16ToFloat64, scaler...)
}

// snip32u creates modbus operation for double register
func (p *SKD045MProducer) snip16u(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 2, RTUUint32ToFloat64, scaler...)
}

// snip32u creates modbus operation for double register
func (p *SKD045MProducer) snip32u(iec Measurement, scaler ...float64) Operation {
	return p.snip(iec, 2, RTUUint32ToFloat64, scaler...)
}

// snip32i creates modbus operation for double register
func (p *SKD045MProducer) snip32i(iec Measurement, scaler ...float64) Operation {
	return p.snip(iec, 2, RTUInt32ToFloat64, scaler...)
}

// snip32f creates modbus operation for double register
func (p *SKD045MProducer) snip32f(iec Measurement, scaler ...float64) Operation {
        return p.snip(iec, 2, RTUIeee754ToFloat64, scaler...)
}

// Probe implements Producer interface
func (p *SKD045MProducer) Probe() Operation {
	return p.snip32f(VoltageL1, 1)
}

// Produce implements Producer interface
func (p *SKD045MProducer) Produce() (res []Operation) {
	  for _, op := range []Measurement{
		  // VoltageL1,VoltageL2,VoltageL3,Power,Frequency,ApparentPower,CurrentL1,CurrentL2,CurrentL3,
		  Voltage, Current, Power, Sum, Frequency,
        } {
		res = append(res, p.snip32f(op, 1))
        }
/*
	for _, op := range []Measurement{
		Current,Power,Sum,
               
	} {
		res = append(res, p.snip32i(op, 10))
	}
	*/
	return res
}
